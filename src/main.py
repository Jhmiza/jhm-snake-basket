from monkey.ioc.core import Registry

registry = Registry();
registry.load('monkey-config.json');
my_object = registry.get("city");
print("hello world, you're in " + my_object + "!");
